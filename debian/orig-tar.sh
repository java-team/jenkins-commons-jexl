#!/bin/sh -e

VERSION=$2
TAR=../jenkins-commons-jexl_$VERSION.orig.tar.gz
DIR=jenkins-commons-jexl-$VERSION
mkdir -p $DIR
# Expand the upstream tarball
tar -xzf $TAR -C $DIR --strip-components=1
# Repack excluding stuff we don't need
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' \
    --exclude 'CVS' --exclude '.svn' $DIR
rm -rf $DIR
